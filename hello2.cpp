////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello2.cpp
/// @version 1.0
///
/// @author Creel Patrocinio <creel@hawaii.edu>
///
///  @date 3/2/2022
///
//////////////////////////////////////////////////////////////

#include <iostream>

int main() {
   std::cout << "hello world!" << std::endl;
       return 0;


}

