////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello1.cpp
/// @version 1.0
///
/// @author Creel Patrocinio <creel@hawaii.edu>
///
//  @date 3/2/2022
//
///////////////////////////////////////////////////////////

#include <iostream>
using namespace std;

int main() {
   cout << "hello world!" << endl;
   return 0;


}

