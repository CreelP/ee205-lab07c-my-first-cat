////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07c - My First Cat - EE 205 - Spr 2022
//
/// @file hello3.cpp
/// @version 1.0
///
/// @author Creel Patrocinio <creel@hawaii.edu>
///
///  @date 3/2/2022
///
/////////////////////////////////////////////////////////////

#include <iostream>



class Cat {
   public:
       void sayHello() {
             using namespace std;
             cout << "meow meow" << endl;

       }
};


int main() {
   Cat myCat;
   myCat.sayHello();
   return 0;

}




